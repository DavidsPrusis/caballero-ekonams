function initMap() {
  let options = {
    zoom: 4,
    center: { lat: 57.0, lng: 14.5 },
  };
  map = new google.maps.Map(document.getElementById("map"), options);

  const script = document.createElement("script");

  script.src =
    "https://developers.google.com/maps/documentation/javascript/examples/json/earthquake_GeoJSONP.js";
  document.getElementsByTagName("head")[0].appendChild(script);
}

const eqfeed_callback = function (results) {
  const location = [
    { lat: 57.0, lng: 14.5 },
    { lat: 52.3, lng: 4.94 },
    { lat: 59.51, lng: 8.48 },
    { lat: 57, lng: 24.19 },
    { lat: 54.94, lng: 23.91 },
    { lat: 58.7, lng: 25.8 },
    { lat: 38.7, lng: 15.8 },
    { lat: 18.7, lng: 45.8 },
    { lat: 48.7, lng: 95.8 },
    { lat: -31.56391, lng: 147.154312 },
    { lat: -33.718234, lng: 150.363181 },
    { lat: -33.727111, lng: 150.371124 },
    { lat: -33.848588, lng: 151.209834 },
    { lat: -33.851702, lng: 151.216968 },
    { lat: -34.671264, lng: 150.863657 },
    { lat: -35.304724, lng: 148.662905 },
    { lat: -36.817685, lng: 175.699196 },
    { lat: -36.828611, lng: 175.790222 },
    { lat: -37.75, lng: 145.116667 },
    { lat: -37.759859, lng: 145.128708 },
    { lat: -37.765015, lng: 145.133858 },
    { lat: -37.770104, lng: 145.143299 },
    { lat: -37.7737, lng: 145.145187 },
    { lat: -37.774785, lng: 145.137978 },
    { lat: -37.819616, lng: 144.968119 },
    { lat: -38.330766, lng: 144.695692 },
    { lat: -39.927193, lng: 175.053218 },
    { lat: -41.330162, lng: 174.865694 },
    { lat: -42.734358, lng: 147.439506 },
    { lat: -42.734358, lng: 147.501315 },
    { lat: -42.735258, lng: 147.438 },
    { lat: -43.999792, lng: 170.463352 },
  ];

  const markers = location.map((location, i) => {
    return new google.maps.Marker({
      position: location,
    });
  });

  var mcOptions = {
    textColor: "white",
    imagePath:
      "https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m",
  };

  new MarkerClusterer(map, markers, mcOptions);
};
