const { src, dest, series, watch } = require("gulp");
const sass = require("gulp-sass");
const imagemin = require("gulp-imagemin");
const uglify = require("gulp-uglify");
const concat = require("gulp-concat");
const browserSync = require("browser-sync").create();

function stylesTask() {
  return src("src/sass/*.scss")
    .pipe(sass())
    .pipe(dest("public/css"))
    .pipe(browserSync.stream());
}

function copyHTMLTask() {
  return src("src/*.html").pipe(dest("public")).pipe(browserSync.stream());
}
function jsConcatTask() {
  return src("src/js/*.js")
    .pipe(concat("main.js"))
    .pipe(uglify())
    .pipe(dest("public/js"))
    .pipe(browserSync.stream());
}

function copyVideo() {
  return src("src/assets/videos/*").pipe(dest("public/videos"));
}

function imageTask() {
  return src("src/assets/images/*")
    .pipe(imagemin())
    .pipe(dest("public/images"));
}

function browsersyncServe(cb) {
  browserSync.init({
    server: { baseDir: "public" },
  });

  watch("src/sass/*.scss").on("change", stylesTask);
  watch("src/*.html").on("change", copyHTMLTask);
  watch("src/js/*.js").on("change", jsConcatTask);
}

exports.default = series(
  stylesTask,
  copyHTMLTask,
  copyVideo,
  imageTask,
  jsConcatTask,
  browsersyncServe
);
